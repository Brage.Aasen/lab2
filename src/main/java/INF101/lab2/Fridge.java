package INF101.lab2;

import java.util.ArrayList;
import java.util.List;

public class Fridge implements IFridge {

    int max_size = 20;
    ArrayList<FridgeItem> items = new ArrayList<>();
    
    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (items.size() >= max_size) {
            return false;
        }
        return items.add(item);
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (items.size() == 0) {
            throw new java.util.NoSuchElementException("Fridge is empty");
        }
        else if (!items.contains(item)) {
            throw new IllegalArgumentException("The item is not in the fridge");
        }

        items.remove(item);
    }

    @Override
    public void emptyFridge() {
        items.clear();
    }

    public void takeOutFromEmptyFridgeTest() {
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> ExpiredItems = new ArrayList<>();
        for (FridgeItem item : items) { 
            if (item.hasExpired()) {
                ExpiredItems.add(item);
            }
        }
        items.removeAll(ExpiredItems);
        return ExpiredItems;
    }

}
